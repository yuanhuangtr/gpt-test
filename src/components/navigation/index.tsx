"use client";
import * as React from "react";

import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import ChatIcon from "@mui/icons-material/Chat";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import StarBorder from "@mui/icons-material/StarBorder";
import StorageOutlinedIcon from "@mui/icons-material/StorageOutlined";
import { useRouter } from "next/navigation";
import { OpenAIClient } from "@/src/utils/OpenAIClient";
import { useEffect } from "react";
import { Box } from "@mui/material";

const Navigation: React.FC = () => {
  const [open, setOpen] = React.useState(true);

  const router = useRouter();

  const client = new OpenAIClient().client;

  useEffect(() => {
    client.listModels().then((res) => {
      console.log(res);
    });
  }, []);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <Box sx={{ width: 320, minWidth: 320, borderRight: "1px solid rgba(0, 0, 0, 0.12)" }}>
      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            GPT Example List
          </ListSubheader>
        }
      >
        <ListItemButton>
          <ListItemIcon>
            <StorageOutlinedIcon />
          </ListItemIcon>
          <ListItemText
            primary="Engine List"
            onClick={() => router.push("/examples")}
          />
        </ListItemButton>
        <ListItemButton>
          <ListItemIcon>
            <ChatIcon />
          </ListItemIcon>
          <ListItemText
            primary="Chart"
            onClick={() => router.push("/examples/chart")}
          />
        </ListItemButton>
        <ListItemButton onClick={handleClick}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Inbox" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon>
                <StarBorder />
              </ListItemIcon>
              <ListItemText primary="Starred" />
            </ListItemButton>
          </List>
        </Collapse>
      </List>
    </Box>
  );
};

export default Navigation;
