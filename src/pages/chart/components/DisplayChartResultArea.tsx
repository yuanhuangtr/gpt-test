import React from "react";
import { createId } from '@paralleldrive/cuid2';
import { Container } from "@mui/material";
import clsx from "clsx";
import DisplayChartItem from "@/src/pages/chart/components/DisplayChartItem";

export interface DisplayChartResultAreaProps {
  contexts: string[];
}

const DisplayChartResultArea: React.FC = ({
  contexts,
}: DisplayChartResultAreaProps) => {
  return (
    <Container maxWidth="md" sx={{ height: "calc(100vh - 200px)" }}>
      <div
        style={{ background: "#f1f1f1" }}
        className={clsx("flex", "flex-col", "h-full")}
      >
        {contexts.map((val, idx) => {
          return <DisplayChartItem text={val} self={idx % 2 !== 0} key={createId()} />;
        })}
      </div>
    </Container>
  );
};

export default DisplayChartResultArea;
