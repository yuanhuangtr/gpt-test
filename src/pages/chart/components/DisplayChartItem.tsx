import React from "react";
import { Paper } from "@mui/material";
import clsx from "clsx";

export interface DisplayChartItemProps {
  text: string;
  self?: boolean;
}

const DisplayChartItem: React.FC<DisplayChartItemProps> = ({
  text,
  self,
}: DisplayChartItemProps) => {
  return (
    <div className={clsx("flex","m-1", { "flex-row-reverse": self })}>
      <Paper
        elevation={3}
        sx={{
          width: 600,
          padding: 2,
          marginBottom: 1,
          bgcolor: self ? "#98FB98" : "",
        }}
      >
        {text}
      </Paper>
    </div>
  );
};

export default DisplayChartItem;
