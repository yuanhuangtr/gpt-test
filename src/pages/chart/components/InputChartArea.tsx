import React, { useState } from "react";
import {Button, Container, TextField} from "@mui/material";
import clsx from "clsx";


export interface InputChartAreaProps {
  onSubmit: (text: string) => void;
  loading?: boolean;
}

const InputChartArea: React.FC = ({ onSubmit, loading }: InputChartAreaProps) => {
  const [text, setText] = useState<string>("");

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  };

  const handleClearClick = ()=>{
    setText("");
  }

  const handleSubmitClick = ()=>{
    onSubmit(text);
    setText("");
  }

  const handleKeyDown = (e:React.KeyboardEvent)=>{

    if(e.key ==="Enter") {
      handleSubmitClick();
    }
  }

  return (
    <Container maxWidth="md" className={"mt-4"}>
      <TextField
        label="tell me your idea"
        placeholder="say something"
        className={clsx("w-160")}
        multiline
        sx={{width:'800px'}}
        value={text}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
      />
      <div className={clsx("flex", "mt-3", "flex-row-reverse","gap-x-4")}>
        <Button variant="contained" onClick={handleSubmitClick} disabled={loading}>Submit</Button>
        <Button variant="outlined" onClick={handleClearClick}disabled={loading}>Clear</Button>
      </div>
    </Container>
  );
};

export default InputChartArea;
