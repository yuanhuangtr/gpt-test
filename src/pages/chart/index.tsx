"use client";

import React, { useState } from "react";
import InputChartArea from "@/src/pages/chart/components/InputChartArea";
import DisplayChartResultArea from "@/src/pages/chart/components/DisplayChartResultArea";
import { fetchPost } from "@/src/utils/fetchPost";

const ChartPage: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const [contexts, setContexts] = useState<string[]>([]);

  const handleSubmit = async (val: string) => {
    setLoading(true);
    const currentContext= [...contexts, val];
    setContexts(currentContext);
    const response = await fetchPost("/api/chart", { text: val });
    setContexts(() => [...currentContext, response.res]);
    setLoading(() => false);
  };

  return (
    <div>
      <DisplayChartResultArea contexts={contexts} />
      <InputChartArea onSubmit={handleSubmit} loading={loading} />
    </div>
  );
};

export default ChartPage;
