import { Configuration, OpenAIApi } from "openai";

export class OpenAIClient {
  private static configuration: Configuration;
  private static client: OpenAIApi;

  constructor() {
    if (!this.client) {
      OpenAIClient.configuration = new Configuration({
        organization: process.env.NEXT_PUBLIC_ORGANIZATION,
        apiKey: process.env.NEXT_PUBLIC_OPENAI_API_KEY,
      });
      OpenAIClient.client = new OpenAIApi(OpenAIClient.configuration);
    }
  }

  get client() {
    return OpenAIClient.client;
  }

}
