import { OpenAIClient } from "@/src/utils/OpenAIClient";

export async function POST(request: Request) {
  const { text } = await request.json();

  const { client } = new OpenAIClient();
  const response = await client.createChatCompletion({
    messages: [{ role: "user", content: text }],
    model: "gpt-3.5-turbo-0301",
  });

  return new Response(JSON.stringify({ res: response.data.choices[0].message.content }));
}
