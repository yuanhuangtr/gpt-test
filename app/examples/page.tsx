import {OpenAIClient} from "@/src/utils/OpenAIClient";


const getData = async ()=>{
  const {client} = new OpenAIClient();
  return client.listModels();
}

export default async function Example() {

  const data = await getData();

  return (
    <div >
      {JSON.stringify(data.data, null, 2)}
    </div>
  )
}
