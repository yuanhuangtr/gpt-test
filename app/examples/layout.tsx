import Navigation from "@/src/components/navigation";
import React from "react";
import clsx from "clsx";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className={clsx("flex", "h-screen")}>
      <Navigation />
      <div className={clsx("p-8")}>{children}</div>
    </div>
  );
}
